import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';



@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit {

  events: calendarI[] = [
    { title: 'Present', date: '2022-06-01', color: '#0000ff' },
    { title: 'absent', date: '2022-06-02', color: '#ff0000' }
  ];

  listacitas!: citaI[];
  mensaje!: string;
  agradecimiento: string ='¡¡¡Su cita ha sido agendada con éxito!!!'
  EJnombre!: string;

  formulario!: FormGroup;
  constructor(private fb: FormBuilder,
    private router: Router) { 

    this.crearFormulario()
    // console.log(this.datos);
    
  }

  crearFormulario(): void{
    this.formulario = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      apellido: ['', [Validators.required, Validators.minLength(3)]],
      correo: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      telefono: ['', [Validators.required, Validators.minLength(8)]],
      comentario: ['', [Validators.required, Validators.minLength(7)]],
      lugar: ['', Validators.required],
      hora: ['', Validators.required],
      fecha: ['', Validators.required]
    });
  }

  
  //GET
  get nombreNoValido(){
    return this.formulario.get('nombre')?.invalid && this.formulario.get('nombre')?.touched;
  }

  get apellidoNoValido(){
    return this.formulario.get('apellido')?.invalid && this.formulario.get('apellido')?.touched;
  }

  get correoNoValido(){
    return this.formulario.get('correo')?.invalid && this.formulario.get('correo')?.touched
  }

  get telefonoNoValido(){
    return this.formulario.get('telefono')?.invalid && this.formulario.get('telefono')?.touched
  }


  ngOnInit(): void {
  }

  guardar(): void{
        // console.log(this.formulario.value);
        this.añadirInfo();
        this.mensaje = this.agradecimiento

    
        this.formulario.reset()
        setTimeout(() => {
          this.mensaje = ''
        }, 3000);
      }
    
  añadirInfo(){
    const user: citaI = {
      nombre: this.formulario.value.nombre,
      apellido: this.formulario.value.apellido,
      telefono: this.formulario.value.telefono,
      correo: this.formulario.value.correo,
      comentario: this.formulario.value.comentario,
      fecha: this.formulario.value.fecha,
      hora: this.formulario.value.hora
    }
    console.log(user);
    
    const calendar: calendarI  = {
      title: this.formulario.value.nombre,
      date: this.formulario.value.fecha,
      color: '#666'
    }
    // this.agregarUsuario(calendar)
    this.agregarTabla(user)
  }
    
  agregarUsuario(cita: calendarI): void {
    this.events.unshift(cita);
    console.log(this.events, 'alba');
    // this.calendarOptions.events = this.events

  }

  agregarTabla(cita: citaI){
    
    if(localStorage.getItem('Citas') === null){
      this.listacitas = [];
      this.listacitas.unshift(cita);
      localStorage.setItem('Citas', JSON.stringify(this.listacitas))
    } else {
      this.listacitas = JSON.parse(localStorage.getItem('Citas') || '');
      this.listacitas.unshift(cita);
      localStorage.setItem('Citas', JSON.stringify(this.listacitas))
    }
    console.log(this.listacitas, 'lista de agregarTablas');
    this.formulario.reset()
  }

  // eliminarCitaU(cita: string){
  //   console.log('intento eliminar');
    
  //   this.eliminarcita(cita);
  //   this.cargarCita();

  // }

  eliminarcita(cita: citaI){
  console.log('intento eliminar');
  console.log(cita, 'elimiar');
  const response = confirm('Estás seguro que deseas eliminar la cita?')

  if(response){
    for(let i = 0; i < this.listacitas.length; i ++){
      if(cita == this.listacitas[i]){
        console.log('eli');
        
        this.listacitas.splice(i, 1);
        localStorage.setItem('Citas', JSON.stringify(this.listacitas))
      }
    }
    
  }

    this.listacitas = this.listacitas.filter(data => {
        return data.nombre!== cita.nombre
    })
  }

  getcita(): calendarI[]{
    return this.events.slice()
    
  }

  obtenerLocalStorage(){
    let citaLista = JSON.parse(localStorage.getItem("Citas") || 'asd');
    console.log(citaLista);
    
    this.listacitas = citaLista
    console.log(citaLista, 'obtener LocalStorage');
  }

 
  verCita(cita: string){

  }

  modificarCita(cita: string){
    console.log(cita, 'Modificacion');
    this.router.navigate(['/agenda/editar', cita])

  }
  MensajeEnviado(): void{
    this.formulario.reset()

    this.mensaje = this.agradecimiento
    setTimeout(() => {
      this.mensaje = ''
    }, 3000);
  }

  modificarUsuario(usuario: string){
    console.log(usuario);
    this.router.navigate(['dashboard/crear-usuario', usuario]);
  }

}

interface citaI{
  nombre: string,
  apellido: string,
  correo: string,
  telefono: number,
  comentario: string,
  fecha: string,
  hora: string
}

interface calendarI {
  title: string,
  date: string,
  color: string
}